import './App.scss';
import SelectBox from './components/SelectBox';
import WeatherInformation from './components/WeatherInformation';

function App() {
  return (
    <div className="App">
      <SelectBox/>
      <WeatherInformation/>
    </div>
  );
}

export default App;
