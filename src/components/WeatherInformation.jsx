import { Button } from "@material-ui/core";
import React, { useContext, useState } from "react";
import { CityContext } from "../contexts/city";
import styles from "./WeatherInformation.module.scss";

function WeatherInformation() {
  const [{ city, data }] = useContext(CityContext);
  const [cityMood, setCityMood] = useState({});

  console.log(cityMood[city]);
  console.log(data);

  return (
    <div>
      {data && (
        <div className={styles.container}>
          <div>
            <div className={styles.box}>Temperature: {data.temp_c} C</div>
            <div className={styles.box}>
              {data.condition.text} <img src={data.condition.icon} />
            </div>
            <div className={styles.box}>Wind Speed: {data.wind_kph} kph</div>
          </div>
          <div>
            <div className={styles.box}>Pressure: {data.pressure_mb} mb</div>
            <div className={styles.box}>Humidity: {data.humidity} %</div>
            <div className={styles.box}>Visibility: {data.vis_km} km</div>
          </div>
        </div>
      )}

      {city && (
        <div className={styles.buttonContainer}>
          <div className={styles.button}>
            <Button
              variant="contained"
              color={cityMood[city] === "Great!" ? "secondary" : "primary"}
              onClick={() => setCityMood((cm) => ({ ...cm, [city]: "Great!" }))}
            >
              Great!
            </Button>
          </div>
          <div className={styles.button}>
            <Button
              variant="contained"
              color={
                cityMood[city] === "Not so great!" ? "secondary" : "primary"
              }
              onClick={() =>
                setCityMood((cm) => ({ ...cm, [city]: "Not so great!" }))
              }
            >
              Not so great!
            </Button>
          </div>
          <div className={styles.button}>
            <Button
              variant="contained"
              color={cityMood[city] === "Okay-ish!" ? "secondary" : "primary"}
              onClick={() =>
                setCityMood((cm) => ({ ...cm, [city]: "Okay-ish!" }))
              }
            >
              Okay-ish!
            </Button>
          </div>
          <div className={styles.button}>
            <Button
              variant="contained"
              color={cityMood[city] === "Sad!" ? "secondary" : "primary"}
              onClick={() => setCityMood((cm) => ({ ...cm, [city]: "Sad!" }))}
            >
              Sad!
            </Button>
          </div>
        </div>
      )}
    </div>
  );
}

export default WeatherInformation;
