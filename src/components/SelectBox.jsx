import React, { useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { CityContext } from "../contexts/city";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  button: {
    display: "block",
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

export default function ControlledOpenSelect() {
  const [{ city }, setData, setCity] = useContext(CityContext);
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    setCity(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  useEffect(() => {
    if (city)
      axios
        .get(
          `https://api.weatherapi.com/v1/current.json?key=5305e2d1baf1405ca3a151843210508&q=${city}&aqi=no`
        )
        .then((res) => {
          console.log(res);
          setData(res.data.current);
        })
        .catch((err) => alert(err));
  }, [city]);

  return (
    <div>
      <h2>Select City For Weather Informations</h2>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-controlled-open-select-label">
          Select City
        </InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={city}
          onClick={handleChange}
        >
          <MenuItem value={"Rijeka"}>Rijeka</MenuItem>
          <MenuItem value={"Zagreb"}>Zagreb</MenuItem>
          <MenuItem value={"Split"}>Split</MenuItem>
          <MenuItem value={"Rome"}>Rome</MenuItem>
          <MenuItem value={"Berlin"}>Berlin</MenuItem>
          <MenuItem value={"Helsinki"}>Helsinki</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}
