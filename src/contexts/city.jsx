import { createContext, useState } from "react";

export const CityContext = createContext();

export const CityProvider = ({ children }) => {
  const [city, setCity] = useState("");
  const [data, setData] = useState();

  return (
    <CityContext.Provider value={[{ city, data }, setData, setCity]}>
      {children}
    </CityContext.Provider>
  );
};
